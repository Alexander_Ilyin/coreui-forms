declare module getTestDomElementModule {
    interface Static {
        (): HTMLElement;
    }
}

declare module "coreui-forms/tests/getTestDomElement" {
    var x : getTestDomElementModule.Static;
    export  = x;
}

declare module "coreui-forms/src/autocompleteEditor" {
    import {Component} from "react";
    class ModuleClass extends Component<any, any> {     }
    export = ModuleClass;
}

declare module "coreui-forms/src/radioGroupEditor" {
    import {Component} from "react";
    class RadioGroupEditor extends Component<any, any> {     }
    export = RadioGroupEditor;
}

declare module "coreui-forms/src/textEditor" {
    import {Component} from "react";
    class TextEditor extends Component<any, any> {     }
    export = TextEditor;
}
declare module "coreui-forms/src/checkboxEditor" {
    import {Component} from "react";
    class CheckboxEditor extends Component<any, any> {     }
    export = CheckboxEditor;
}

declare module "coreui-forms/src/tabSelectEditor" {
    import {Component} from "react";
    class ModuleClass extends Component<any, any> {     }
    export = ModuleClass;
}

declare module "coreui-forms/src/tabSelect" {
    import {Component} from "react";
    class TabSelect extends Component<any, any> {     }
    class TabSelectItem extends Component<any, any> {     }
}

declare module "coreui-forms/src/lightbox/loading" {
    import {Component} from "react";
    class Loading extends Component<any, any> {     }
    export = Loading;
}


declare module "coreui-forms/src/common/domUtils" {
    class DomUtils  {
        static refreshLoop();
        static addEventListener(el:any,eventName:string, handler:any);
        static removeEventListener(el:any,eventName:string, handler:any);
        static IEVersion():number
    }
    export = DomUtils;
}

declare module "coreui-forms/src/lightbox" {

    import {Component} from "react";
    class Body extends Component<any, any> {     }
    class Header extends Component<any, any> {     }
    class Footer extends Component<any, any> {     }
    class Lightbox extends Component<any, any> {
        static show(content:JSX.Element);
        static closeAll();
    }
    interface ShowResult{
        contentNode : any,
        promise:Promise<any>,
        close : any,
        cancel : any,
    }
    function show(content:JSX.Element):ShowResult;
}



declare module "coreui-forms/src/wizard" {
    import {Component} from "react";

    class Wizard extends Component<any, any> {
        show(content:JSX.Element):void;
        close():void;
        cancel():void;
    }
    export = Wizard;

}


declare module "coreui-forms/src/formComponent" {
    import {Component} from "react";
    class FormContext<T>{}
    
    class Forms {
        child<T>(v:string) : FormContext<T>;
        child<T>(v:T):FormContext<T>;
        activateAllValidation();
        getModelValue():any;
        isExpired():boolean;
    }

    export = class FormComponent<TModel, TProps, TState> extends Component<TProps, TState> {
        model() : TModel;
        forms() : Forms;
        componentWillMount();
        componentWillUnmount();

        componentWillUpdate() ;
    }
}

declare module "coreui-forms/tests/testHelper" {
    class TestHelper {
        constructor(dom, parent?:TestHelper);
        checkFocused($el:any, val) : TestHelper
        checkVal($el:any, val) : TestHelper
        checkHidden($el:any) : TestHelper
        check($el:any, callback) : TestHelper
        enqueue(f:Function) : TestHelper
        click($el:any) : TestHelper
        checkEnabled($el:any) : TestHelper
        type($el:any, text) : TestHelper
        checkVisible($el:any) : TestHelper
        focus($el:any) : TestHelper
        blur($el:any) : TestHelper
        find(selector:any) : any
        keyDown($el:any, key, count) : TestHelper
    }
    export = TestHelper;
}


