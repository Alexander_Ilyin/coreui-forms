var React = require("react");
var ReactDOM = require("react-dom");
var forms = require("../common/mixin.jsx");
var Input = require("retail-ui/components/Input");
var Autocomplete = require("../autocomplete");
var Group = require("retail-ui/components/Group");
var Button = require("retail-ui/components/Button");
var Icon = require("retail-ui/components/Icon");
var Base = require("coreui-models/src/base");
var _ = require("underscore");
var log = function(a){return console.log(a);};

var AutocompleteEditor = React.createClass({
    mixins: [forms],
    getInitialState(){ return {}},

    componentDidMount() {
        this.forms().onChanged(this._importStateFromModel);
        this._importStateFromModel();
    },
    
    componentWillUnmount() {
        this.forms().unChanged(this._importStateFromModel);
    },

    render() {
        var error = this.state.error;
        var title = error != null ? error.msg : this.props.title;
        var autoComplete = <Autocomplete
            {...this.props}
            error={error!=null}
            title={title}
            onFocus={this._onFocus}
            onBlur={this._onBlur}
            item={this.state.item}
            value={this.state.text}
            width={this.props.width}
            source={this.props.source}
            autoSelect={this.props.autoSelect}
            displayField={this.props.displayField}
            onChange={this._onChange}
            ref="autoComplete"/>;
        return autoComplete;
    },

    _onFocus : function(){
        this._getErrorTooltip().update({visible:true});
    },
    _onBlur : function(){
        this.forms().activateValidation();
        this._getErrorTooltip().update({visible:false});
    },

    _getErrorTooltip : function(){
        return this.forms().getErrorTooltip({
            findTarget : ()=>this.refs.autoComplete ? ReactDOM.findDOMNode(this.refs.autoComplete) : null
        });
    },

    _onChange : function(text, item){
        this._getErrorTooltip().update({
            error: this.forms().getActiveError()
        });

        var val = this._getValue(item, text);
        this._currentValue = val;
        this.forms().setModelValue(val);
        this.setState({
            item : item,
            text: text
        });
    },

    _getValue(item, text) {
        if (this.props.getValue && (this.props.getValueSupportsNull || item))
            return this.props.getValue(item, text);
        if (this.props.valueField && item)
            return item[this.props.valueField];
        return item;
    },

    _importStateFromModel : function() {
        var error = this.forms().getActiveError();
        this._getErrorTooltip().update({error: error});
        this.setState({error : error });

        var modelValue = this.forms().getModelValue();


        if (!_.isEqual(this._currentValue, modelValue)){
            if (this.props.findItem || this.props.findItem == 'auto') {
                if (modelValue) {
                    this.findItem(modelValue).then((item)=>{
                        this._currentValue = item;
                        if (this.refs.autoComplete) {
                            var text = this.refs.autoComplete._getDisplayText(item);
                            this.refs.autoComplete.setState({
                                value: item ? text : ""});
                            this.setState({
                                item : item,
                                text : text
                            });
                        }
                    });
                }
                else {
                    if (this.refs.autoComplete)
                        this.refs.autoComplete.setState({value: ""});
                    this.setState({
                        item : null,
                        text : ""
                    });
                }
            }
        }
    },

    findItem(modelValue) {
        if (this.props.findItem && _.isFunction(this.props.findItem))
            return this.props.findItem(modelValue);

        if (this.props.findItem == 'auto')
            return this.props.source().then(items=> {
                var item = _.find(items, x=>this._getValue(x) == modelValue);
                return item;
            });
        return null;
    }
});

module.exports.AutocompleteEditor = AutocompleteEditor;