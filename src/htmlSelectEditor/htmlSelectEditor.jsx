var React = require("react");
var ReactDOM = require("react-dom");
var forms = require("../common/mixin.jsx");
var Input = require("retail-ui/components/Input");
var DatePicker = require("retail-ui/components/DatePicker");
var Group = require("retail-ui/components/Group");
var Button = require("retail-ui/components/Button");
var Icon = require("retail-ui/components/Icon");
var Base = require("coreui-models/src/base");
var HtmlSelect = require("../htmlSelect");
var _ = require("underscore");
var log = function(a){return console.log(a);};

var HtmlSelectEditor = React.createClass({
    mixins: [forms],
    getInitialState(){ return {}},

    componentDidMount() {
        this.forms().onChanged(this._importStateFromModel);
        this._importStateFromModel();
    },

    render() {
        return <HtmlSelect
            {...this.props}
            ref='select'
            onFocus={this._onFocus}
            onBlur={this._onBlur}
            onChange={this._onChange}
            value={this.state.value}>{this._renderOptions()}</HtmlSelect>;
    },

    _renderOptions(){
        if (this.props.options!="auto")
            return this.props.children;

        var func = this.forms().model[this.forms().propName];
        var meta = func.meta;
        var res =_.map(meta.getEnumValues(), (v)=><option value={v.key}>{v.value}</option>);
        if (this.props.emptyValue)
            res.unshift(<option value="">{this.props.emptyValue}</option>);
        return res;
    },

    _onFocus : function(){
        this.isFocused = true;
        this._getErrorTooltip().update({visible:true});
    },

    _onBlur : function(){
        this._getErrorTooltip().update({visible:false});
        this.isFocused = false;

    },
    _getErrorTooltip : function(){
        return this.forms().getErrorTooltip({
            findTarget : ()=>{
                var r = this.refs.select ? ReactDOM.findDOMNode(this.refs.select) : null
                console.log(r);
                return r;
            }
        });
    },

    _onChange : function(event){
        var value = event.target.value;
        this.forms().activateValidation();
        this.forms().setModelValue(value);
        var error = this.forms().getActiveError();
        this._getErrorTooltip().update({error: error});
        this.setState({
            error : error,
            value : value
        });
    },

    _importStateFromModel : function() {
        var error = this.forms().getActiveError();
        this._getErrorTooltip().update({error: error});
        var modelValue = this.forms().getModelValue();
        this.setState({
            error : error,
            value : modelValue
        });
    },

    getDefaultProps(){
        return {
           renderItem: this.renderItem
           }
    }
});

module.exports = HtmlSelectEditor;