var React = require("react");
var ReactDOM = require("react-dom");
var forms = require("../common/mixin.jsx");
var Input = require("retail-ui/components/Input");
var DatePicker = require("retail-ui/components/DatePicker");
var Group = require("retail-ui/components/Group");
var Button = require("retail-ui/components/Button");
var Icon = require("retail-ui/components/Icon");
var Base = require("coreui-models/src/base");
var _ = require("underscore");
var log = function(a){return console.log(a);};

var DateEditor = React.createClass({
    mixins: [forms],
    getInitialState(){ return {}},

    componentDidMount() {
        this.forms().onChanged(this._importStateFromModel);
        this._importStateFromModel();
    },

    render() {
        return <DatePicker
            {...this.props}
            error={this.state.error!=null}
            onFocus={this._onFocus}
            onBlur={this._onBlur}
            width={this.props.width}
            onChange={this._onChange}
            value={this.state.value}
            ref="datePicker"/>;
    },

    _onFocus : function(){
        this.isFocused = true;
        this._getErrorTooltip().update({visible:true});
    },

    _onBlur : function(){
        this.forms().activateValidation();
        this._getErrorTooltip().update({visible:false});
        this.isFocused = false;
    },

    _getErrorTooltip : function(){
        return this.forms().getErrorTooltip({
            findTarget : ()=>{
                var r = this.refs.datePicker ? ReactDOM.findDOMNode(this.refs.datePicker) : null
                return r;
            }
        });
    },

    _onChange : function(e){
        this.forms().setModelValue(e.target.value);
    },

    _importStateFromModel : function() {
        var error = this.forms().getActiveError();
        this._getErrorTooltip().update({error: error});
        var modelValue = this.forms().getModelValue();
        this.setState({error : error, value : modelValue});
    }
});

function formatDate(date) {
    if (!date) return '';

    let day = formatNumber(date.getDate());
    let month = formatNumber(date.getMonth() + 1);
    return `${day}.${month}.${date.getFullYear()}`;
}
function formatNumber(value) {
    let ret = value.toString();
    while (ret.length < 2) {
        ret = '0' + ret;
    }
    return ret;
}
module.exports.DateEditor = DateEditor;