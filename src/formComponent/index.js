/**
 * Created by ilyin on 27.10.2015.
 */
var React = require("react");
var forms = require("../common/mixin.jsx");

module.exports = React.createClass({
   mixins:[forms],
   shouldComponentUpdate(){
      if (!this.model())
          return true;

      var expired = this.forms().isExpired();
      return expired;
   },
   render(){}
});