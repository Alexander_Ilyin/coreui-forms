var React = require("react");
var ReactDOM = require("react-dom");
var forms = require("../common/mixin.jsx");
var Input = require("retail-ui/components/Input");
var Group = require("retail-ui/components/Group");
var RadioGroup = require("retail-ui/components/RadioGroup");
var Button = require("retail-ui/components/Button");
var Icon = require("retail-ui/components/Icon");
var Base = require("coreui-models/src/base");
var _ = require("underscore");
var log = function(a){return console.log(a);};

var RadioGroupEditor = React.createClass({
    mixins: [forms],
    getInitialState(){ return {}},

    componentDidMount() {
        this.forms().onChanged(this._importStateFromModel);
        this._importStateFromModel();
    },

    render() {
        return <RadioGroup
            {...this.props} error={this.state.error!=null}
            onChange={this._onChange} value={this.state.value}
            ref="mainElement"/>;
    },

    _onFocus : function(){
        this.isFocused = true;
        this._getErrorTooltip().update({visible:true});
    },

    _onBlur : function(){
        this.forms().activateValidation();
        this._getErrorTooltip().update({visible:false});
        this.isFocused = false;
    },

    _getErrorTooltip : function(){
        return this.forms().getErrorTooltip({
            findTarget : ()=>{
                var r = this.refs.mainElement ? ReactDOM.findDOMNode(this.refs.mainElement) : null
                return r;
            }
        });
    },

    _onChange : function(e){
        this.forms().setModelValue(e.target.value);
    },

    _importStateFromModel : function() {
        var error = this.forms().getActiveError();
        this._getErrorTooltip().update({error: error});
        var modelValue = this.forms().getModelValue();
        this.setState({error : error, value : modelValue});
    }
});

module.exports.RadioGroupEditor = RadioGroupEditor;