var React = require("react");
var Mixin = require("../mixin");
var Checkbox = require("retail-ui/components/Checkbox");

var CheckboxEditor = React.createClass({
    mixins: [Mixin],
    render() {
        var v = this.forms().getModelValue();
        return (<Checkbox checked={v} {...this.props} onChange={this.onChange} {...this.props}/>);
    },
    onChange: function (e) {
        var v = e.target.checked;
        this.forms().setModelValue(v);
        if (this.props.changeListener)
            this.props.changeListener(v);
    }
});
module.exports = CheckboxEditor;