var React = require("react");
var _ = require("underscore");
import styles from './tabSelect.less';


var TabSelect = React.createClass({
    render() {
        var clones = [];
        for (var i = 0; i < this.props.children.length; i++) {
            var tabItem = React.cloneElement(this.props.children[i], {
                isLast : i==(this.props.children.length-1),
                isFirst : i==0,
            })
            clones.push(tabItem);
        }
        return <div style={{width: this.props.width}}>
            <div style={{display:"table", width:"100%"}} className="tabSelectTable">
                {clones}
            </div>
        </div>;
    }
});
var TabSelectItem = React.createClass({
    render : function() {
       return  <div
           style={{display:"table-cell", width:this.props.width}}
           data-x-selected={this.props.selected ? "true" : "false"}
           role={this.props.role}
           className={
                styles.tabSelectItem + 
                " " + (this.props.selected && styles.tabSelectItem_selected_yes) + 
                " " + (!this.props.selected && styles.tabSelectItem_selected_no) +
                " " + (this.props.isLast && styles.tabSelectItem_last_yes) +
                " " + (this.props.isFirst && styles.tabSelectItem_first_yes)
            }

           onClick={this.props.onClick}>
           {this.props.text}
        </div>;
    }
});

module.exports.TabSelect = TabSelect;
module.exports.TabSelectItem = TabSelectItem;