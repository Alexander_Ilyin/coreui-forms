require('./Autocomplete.less');
var React = require('react');
var ReactDOM = require('react-dom');
var Input = require('retail-ui/components/Input');
var AutocompleteMenu = require('./AutocompleteMenu');
var FloatPanelManager = require('../floatPanelManager');
var DomUtils = require('../floatPanelManager/domUtils');
var cx = require('../cx')('RTAutocomplete');
var classNames = require('classnames');
var _ = require('underscore');
var utils = require('../common/utils');
import styles from './autocompleteSelect.less';

var Autocomplete = React.createClass({
  propTypes: {
    mode: React.PropTypes.oneOf(['input', 'input-select', 'select']),

    autoSelect: React.PropTypes.bool,
    ignoreBlur: React.PropTypes.bool,
    searchInMenu: React.PropTypes.bool,
    error: React.PropTypes.any,
    title: React.PropTypes.string,
    width: React.PropTypes.oneOfType([React.PropTypes.number, React.PropTypes.string]),
    menuWidth: React.PropTypes.oneOfType([React.PropTypes.number, React.PropTypes.string]),

    displayField: React.PropTypes.string,
    item: React.PropTypes.any,
    value: React.PropTypes.any,

    onBlur: React.PropTypes.func,
    onChange: React.PropTypes.func,
    onFocus: React.PropTypes.func,
    getDisplayText: React.PropTypes.func,
    renderItem: React.PropTypes.func,
    source: React.PropTypes.func
  },

  componentWillMount() {
    var self = this;
    this.isFresh= true;
    setTimeout(function() { self.isFresh = false; }, 750);
  },

  getDefaultProps(){
    return {
      mode: "input",
      ignoreBlur : /ignoreBlur/.test(window.location.href)

    }
  },


  getInitialState : function(){

      return {
        value: this.props.value || "",
        item : null

      };
  },

  render() {
    if (this.props.mode=='input' || (this.props.mode=='input-select' && !this.state.item)) {
      return this._renderInput();
    }
    else
      return this._renderToken();
  },

  _renderInput(){
      var inputProps = {
        error: this.props.error,
        title: this.props.title,
        value: this._getDisplayText(this.state.item) || this.state.value,
        onChange: this._handleChange,
        onFocus: this._handleFocus,
        onKeyDown: this._handleKey,
        onBlur: this._handleBlur
      };
      return <Input {...this.props} {...inputProps} ref="input"/>;
  },

  _renderToken() {
    var label;
    var item = this.state.item;
    if (item) {
      label = this._getDisplayText(item);
    }
    else {
      label = (<div className={styles.placeholder}>{this.props.placeholder || 'Значение не выбрано'}</div>);
    }
    var rootProps = {
      className: classNames({
        [styles.root]: true,
        [styles.isOpened]: this.state.opened,
      }),
      tabIndex: (this.state.opened && this.props.search) ? "-1" : "0",
      onKeyDown: this._handleKey,
      onBlur: this._handleBlur,
    };
    var labelProps = {
      className: classNames({
        [styles.label]: true,
        [styles.labelIsOpened]: this.state.opened,
      }),
      onClick: this._handleTokenClick,
    };
    if (this.props.width) {
      rootProps.style = {
        width: this.props.width,
      };
    }
    return (
        <span {...rootProps}>
        <span {...labelProps} role='Token'>
          <span className={styles.labelText}>{label}</span>
          <div className={styles.arrow}/>
        </span>
      </span>
    );
  },

  componentWillReceiveProps(newProps){
    var newState = {};
    if (newProps.value!==undefined)
      newState.value =newProps.value;
    if (newProps.item!==undefined)
      newState.item = newProps.item;
    this.setState(newState);
  },

  focus : function(){
      this.refs.input.focus();
  },

  // hack for IE - autocomplete menu opens on mount
  componentWillUnmount(){
    if (this.menu) {
      this.menu.dispose();
      this.menu = null;
    }
  },

  _handleBlur(event) {
    if (this.props.ignoreBlur || this.__disableBlur || window.__disableBlur)
      return;
    this.__updateMenu({ menuIsVisible : false  });
    this.setState({opened : false});
    if (this.props.onBlur)
      this.props.onBlur(event);
  },

  _handleTokenClick(event){
    this.__updateMenu({ menuIsVisible : true  });
    this.setState({opened : true});


    if (this.props.mode=="input-select") {
      this.setState({item : null}, ()=>this.focus());
      var item = this.state.item;
      this._triggerOnChange(this._getDisplayText(item), null);
    }

    if (this.props.onFocus)
      this.props.onFocus(event);
  },

  _handleFocus(event) {
    this.__updateMenu({ menuIsVisible : true  });
    this.setState({opened : true});

    if (this.props.onFocus)
      this.props.onFocus(event);
  },

  _isAutoSelect: function () {
    return this.props.autoSelect !== false && this.props.mode == 'input';
  },

  _handleChange(e) {

    // hack for IE - autocomplete menu opens on mount
    if (this.isFresh) return;

    var v = e.target.value;
    this.setState({value: v});
    this.menuIsVisible = true;
    if (!this._isAutoSelect())
      this._triggerOnChange(v, []);
    this._load(v);
  },

  _load : utils.throttle_instance(function(v) {
    var autoSelect = this._isAutoSelect();
    this._latestValue = v;
    var items = this._getItems(v);
    if (!items) {
      if (this.menu) {
        this.menu.hide();
        this.menu.content.setItems([]);
      }
      this._triggerOnChange(v, []);
      return;
    }

    items.then((items)=> {
      if (!this.menuIsVisible || v != this._latestValue)
        return;
      if (autoSelect)
        this._triggerOnChange(v, items);
      if (!this.menu)
        this.menu = this._createMenu();
      this.menu.show();
      this.menu.content.setItems(items);
    });
  }, 1000),

  _onItemSelect : function(item){

    var value = this._getDisplayText(item);
    this.setState({value : value});

    this.__updateMenu({ menuIsVisible : false  });
    this.setState({opened : false});
    if (this.props.onChange)
      this.props.onChange(value, item);
  },

  _handleKey(event) {
    var items = this.state.items;
    var stop = false;
    if (this.menu) {
      if (event.key === 'ArrowUp' || event.key === 'ArrowDown' || event.key === 'Enter' || event.key === 'Escape') {
        event.preventDefault();
        stop = true;
        if (event.key === 'ArrowUp') {
          this.menu.content.selectPrev();

        }
        else if (event.key === 'ArrowDown') {
          this.menu.content.selectNext();
        }
        else if (event.key === 'Escape') {
          this.__updateMenu({menuIsVisible: false});
        }
        else if (event.key === 'Enter') {
          this.menu.content.selectCurrent();
        }
      }
    }
  },

  _getDisplayText : function(item){
      if (!item)
        return "";
      if (this.props.getDisplayText)
        return this.props.getDisplayText.call(this, item);

      if (this.props.displayField)
          return item[this.props.displayField];
  },

  _renderItem : function(item){
    if (this.props.renderItem)
      return this.props.renderItem.apply(this, arguments);
    return this._getDisplayText(item);
  },

  _getItems : function(query){
    if (typeof(this.props.source)=="function")
       return this.props.source.call(this, query);
    else
      throw "autocomplete source is invalid";
  },

  __updateMenu : function(cfg) {
    if (cfg.menuIsVisible!==undefined)
      this.menuIsVisible = cfg.menuIsVisible;

    if (!this.menuIsVisible) {
      if (this.menu) {

        this.menu.hide();
      }
      return;
    }

    if (!this.menu) {
      this.menu = this._createMenu();
    }

      var query = this.props.mode=='input' ? this.state.value : undefined;

      this.menu.content.loadItems(query).then(()=>{
            if (!this.menuIsVisible)
              return;

            this.menu.show();
            if (this.props.searchInMenu) {
              this.__disableBlur = true;
              this.menu.content.focus();
              this.menu.content._updateScroll();
              this.__disableBlur = false;
            }
      });
  },

  _createMenu : function() {
    var input = ReactDOM.findDOMNode(this);
    var outerWidth = DomUtils.outerWidth(input);
    var menuWidth = this.props.menuWidth;
    var onItemSelect = this._onItemSelect;
    var renderItem = this._renderItem;
    var searchInMenu = this.props.searchInMenu;
    var _getItems = this._getItems;
    return FloatPanelManager.createPanel({
      render: function () {
        return <AutocompleteMenu
          search={searchInMenu}
          items={[]}
          getItems={_getItems}
          width={menuWidth}
          minWidth={outerWidth || 130}
          renderItem={renderItem}
          onItemSelect={onItemSelect}
          onEscape={()=>this.__updateMenu({menuIsVisible: false})}
          />},
      target: ()=>ReactDOM.findDOMNode(this),
      getPosition : function(cfg){
        return {
          left: cfg.targetRect.left,
          top : cfg.targetRect.top + cfg.targetRect.height + 2
        };
      }
    });
  },

  _triggerOnChange : function(currentValue, items){
    if (!this.props.onChange)
      return;
    var foundItem = _.find(items, (x)=>this._getDisplayText(x)==currentValue);
    this.props.onChange(currentValue, foundItem || null);
  }
});

module.exports = Autocomplete;
