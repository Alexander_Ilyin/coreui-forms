var React = require('react');
var ReactDOM = require('react-dom');
var PropTypes = React.PropTypes;
require('./Autocomplete.less');
var domUtils = require('../common/domUtils');
var cx = require('../cx')('RTAutocomplete');
var Input = require('retail-ui/components/input');
var utils = require('../common/utils');

var AutocompleteMenu = React.createClass({
    getInitialState() {
        return {
           items : this.props && this.props.items || [],
           selectedIndex : 0
        }
    },

    render() {
        var items = this.state.items;
        if (!items.length)
            return <div role='AutocompleteMenu' />
        return (
            <div className={cx('menu') + " ui-AutocompleteMenu"}
                 style={{minWidth: this.props.minWidth, width: this.props.width}}
                 role='AutocompleteMenu'>
                {this.props.search && <div className={cx('menu-input-wrap')}><Input
                    ref='input'
                    value={this.state.inputValue}
                    onKeyDown={this._handleKey}
                    onChange={this._handleChange}
                    className={cx('menu-input')}
                    /></div> }
                <div className={cx('menuItems')}
                     ref='menuWrap'>
                    {items && items.map && items.map((item, i) => {
                        let rootClass = cx({
                            'menu-item': true,
                            "menu-item-selected": i == this.state.selectedIndex || this.state["hover" + i]
                        });
                        return (
                            <div
                                 role='AutocompleteItem'
                                 key={i}
                                 ref={i == this.state.selectedIndex ? 'selected' : null}
                                 aria-selected={ i == this.state.selectedIndex ? 'true' : 'false'}
                                 className={rootClass + " ui-AutocompleteItem"}
                                 onMouseEnter={e => {var x = {};x["hover" + i] = true;this.setState(x)}}
                                 onMouseOut={e => {var x = {};x["hover" + i] = false;this.setState(x)}}
                                 onMouse={e => this.setState({hoverIndex: i})}
                                 onMouseDown={e => this._onItemClick(e, item, i)}>
                                {this.props.renderItem(item)}
                            </div>
                        );
                    })}
                </div>
            </div>
        );
    },

    setItems : function(items) {
        this._updateItems(items);
    },

    focus : function (){
        if (this.refs.input)
            this.refs.input.focus();
    },

    loadItems : function(query){
        if (query===undefined)
            query = this.state.inputValue || "";
        return this._loadItems(query);
    },

    selectPrev  : function(){
      var selectedIndex = --this.state.selectedIndex;
      if (selectedIndex<0)
        selectedIndex = 0;
      this.setState({selectedIndex : selectedIndex}, this._updateScroll);
    },


    selectNext : function(){
      var selectedIndex = ++this.state.selectedIndex;
      if (selectedIndex>=this.state.items.length)
        selectedIndex=this.state.items.length-1;

      this.setState({selectedIndex : selectedIndex}, this._updateScroll);
    },

    select : function(item) {
        if (this.props.onItemSelect)
            this.props.onItemSelect(item);
    },
    selectCurrent: function(){
        var item = this.state.items[this.state.selectedIndex];
        if (!item)
            return;
        this.select(item);
    },

    _handleChange : function(e) {
        this.setState({inputValue: e.target.value});
        this._loadItems(e.target.value);
    },

    _handleKey(event) {
        var items = this.state.items;
        var stop = false;
        if (event.key === 'ArrowUp' || event.key === 'ArrowDown' || event.key === 'Enter' || event.key === 'Escape') {
            event.preventDefault();
            stop = true;
            if (event.key === 'ArrowUp') {
                this.selectPrev();
            }
            else if (event.key === 'ArrowDown') {
                this.selectNext();
            }
            else if (event.key === 'Escape') {
                this.props.onEscape && this.props.onEscape();
            }
            else if (event.key === 'Enter') {
                this.selectCurrent();
            }
        }
    },

    _loadItems : utils.throttle_deferred_instance(function(query){
        this._latestValue = query;
        var items = this.props.getItems(query);
        if(!items)
            return;
        return items.then((items)=> {
           if (this._latestValue == query)
                this._updateItems(items);
        });
    }),


    _updateScroll(){
        if (!this.refs.selected)
            return;
        var menu = ReactDOM.findDOMNode(this.refs.menuWrap);
        var delta = menu.offsetTop;
        var selected = ReactDOM.findDOMNode(this.refs.selected);

        if (menu.scrollTop > selected.offsetTop - delta){
            menu.scrollTop = selected.offsetTop - delta;
        }
        else {
            var menuHeight = domUtils.outerHeight(menu, false);
            if (menu.scrollTop <(selected.offsetTop + selected.offsetHeight - delta - menuHeight))
                menu.scrollTop = selected.offsetTop + selected.offsetHeight- delta - menuHeight;
        }
    },

    _updateItems : function(items){
        var selectedIndex = this.state.selectedIndex;
        if (selectedIndex >= items.length)
            selectedIndex = 0;
        this.setState({
            items : items,
            selectedIndex : selectedIndex
        }, this._updateScroll);
    },

    _onItemClick : function(e,item,i) {
        this.setState({selectedIndex : i});
        e.preventDefault();
        this.select(item);
    }
});


module.exports = AutocompleteMenu;
