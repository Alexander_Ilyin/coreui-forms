var style = require("./lightbox-loading.less");
var css = require("./lightbox-loading.less");
var DomUtils = require("../../common/domUtils");
var React = require('react');
var Lightbox = require('../lightbox');

var Loading = React.createClass({
    render : function(){

        var isIe8 = DomUtils.ie8();

        var img = isIe8 ?
            (<div  className={'loader loader-ie8__medium'}>
                <div className="loader-ie8__medium_gif"></div>
            </div>) :
            (<div  className={'loader loader__medium'}></div>);

        return    <div className="loader-container" style={this.props.style}>
                    {img}
                    <div className="loader-children">
                        {this.props.children || Loading.defaultContent}
                    </div>
                </div>;
    }
});
Loading.defaultContent = "Загрузка...";
module.exports = Loading;