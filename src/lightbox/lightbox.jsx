var _ = require("underscore");
require("./lightbox.less");

var DomUtils = require("../common/domUtils");
var React = require("react");
var ReactDOM = require("react-dom");
var genZIndex = require("../floatPanelManager").genZIndex;
var appendReactToBody = DomUtils.appendReactToBody;
var lightboxes = [];
var LightboxWindow = React.createClass({
    getInitialState(){ return {}; },
    render(){
        return <div className="lightbox-container">
                <div className="lightbox-overlay-background" style={{display: this.state.backgroundHidden ? "none" : ""}} ></div>
                <div className="lightbox-content-wrap">
                    <div className="lightbox-spring"></div>
                    <div className="lightbox-content" ref='content'/>
                </div>
            </div>
    },
    hideBackground : function(){
        this.setState({backgroundHidden : true});
    }
});

var closeAll = function(){
    console.log("close all lightboxes!!!!");
    while(lightboxes.length>0){
        lightboxes[lightboxes.length-1].close();
    }
}

var showLightbox = function(content) {

    DomUtils.addClass(document.body, "lightbox-document-body");
    DomUtils.addClass(document.body.parentNode, "lightbox-document-html");
    if (lightboxes.length==0) {
        document.body.parentNode.overflowSaved = document.body.parentNode.style.overflow;
        document.body.parentNode.style.overflow = "hidden";
    }

    var containerWrap = document.createElement("div");
    DomUtils.addClass(containerWrap, "lightbox-container-wrap");
    containerWrap.style.top = DomUtils.getScrollTop()  + 'px';
    containerWrap.style.zIndex = genZIndex();
    document.body.appendChild(containerWrap);

    var newLightbox = ReactDOM.render(<LightboxWindow />, containerWrap);
    var contentNode = ReactDOM.findDOMNode(newLightbox.refs.content);

    var resolve, reject, promise;
    if (typeof(Promise)!='undefined' && Promise.pending) {
        var defer = Promise.defer();

        promise = defer.promise;
        resolve = function(x){defer.resolve(x)};
        reject = function(x){defer.reject(x)};
    }
    else
    {
        resolve = function(x){};
        reject = function(x){};
        promise = null;
    }

    var callbacks = {
        close : (result)=>{closeLightbox(containerWrap, contentNode);resolve(result);},
        cancel : (error)=>{closeLightbox(containerWrap, contentNode);reject(error);}
    };
    content = React.cloneElement(content, callbacks);

    var rendered = ReactDOM.render(content, contentNode);
    if (rendered._setCloseCallback)
        rendered._setCloseCallback(()=>closeLightbox(containerWrap, contentNode));

    var result = _.extend({}, callbacks, {
        contentNode : contentNode,
        content : rendered,
        promise : promise,
        close : callbacks.close,
        cancel : callbacks.cancel,
        containerWrap: containerWrap
    });
    if (DomUtils.ie8()) {
        DomUtils.addClass(document.body, "fakeClass");
        DomUtils.removeClass(document.body, "fakeClass");
    }
    lightboxes.push(result);
    return result;
};

function closeLightbox(containerWrap, contentNode){
    if (containerWrap.parentNode==null)
        return; // already closed
    
    ReactDOM.unmountComponentAtNode(contentNode);
    ReactDOM.unmountComponentAtNode(containerWrap);
    containerWrap.parentNode.removeChild(containerWrap);
    lightboxes = _.filter(lightboxes, x=>x.containerWrap!=containerWrap);
    if (lightboxes.length==0) {
        DomUtils.removeClass(document.body, "lightbox-document-body");
        DomUtils.removeClass(document.body.parentNode, "lightbox-document-html");
        document.body.parentNode.style.overflow = document.body.parentNode.overflowSaved;
    }
};


var Body = React.createClass({
    render(){
        return <div className="lightbox-body" style={{width:this.props.width}}>
            {this.props.children}
        </div>
    }
});

var Header = React.createClass({
    render(){
        return <div className="lightbox-header" style={{width:this.props.width}}>
            {this.props.children}
        </div>
    }
});

var Footer = React.createClass({
    render(){
        return <div className="lightbox-footer" style={{width:this.props.width}}>
            {this.props.children}
        </div>
    }
});

var top = null;

var Lightbox = React.createClass({

    render() {
        return <div style={{width:this.props.width}}>
            <a className='lightbox-close' style={{display:this.props.closable ? "" : "none"}} onClick={this._onCancelClick}></a>
            {this.props.children}
        </div>;
    },

    _setCloseCallback : function(closeWindow){
        this.closeWindow = closeWindow;
    },

    componentDidMount() {
        top = this;
        this._prev = top;
        window.addEventListener('keydown', this._handleKeydown);
    },

    componentWillUnmount() {
        window.removeEventListener('keydown', this._handleKeydown);
        top = this._prev;
        this._prev = null;
    },

    _handleKeydown(e)  {
        if (top!=this)
            return;
        if (e.keyCode === 27) {
            e.preventDefault();
            if (this.props.closable)
                this._onCancelClick();
        }
    },

    _onCancelClick(){
        if (this.props.cancel)
            this.props.cancel();
    }
});


Lightbox.closeAll = closeAll;
Lightbox.show = showLightbox;
Lightbox.Lightbox = Lightbox;
Lightbox.Body = Body;
Lightbox.Header = Header;
Lightbox.Footer = Footer;

module.exports = Lightbox;
