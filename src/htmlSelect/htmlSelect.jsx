import classNames from 'classnames';
import React, {PropTypes} from 'react';
import ReactDOM from 'react-dom';
import styles from './Select.less';


const Select = React.createClass({
    propTypes: {
        disabled: PropTypes.bool,
        value: PropTypes.any,
        defaultValue: PropTypes.any,
        width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        error: PropTypes.bool,
        onChange: PropTypes.func,
        onBlur: PropTypes.func
    },

    render() {
        var labelProps = {
            className: classNames({
                [styles.root]: true,
                [this.props.className || '']: true,
                [styles.disabled]: this.props.disabled,
                [styles.error]: this.props.error,
            }),
            style: {},
        };
        if (this.props.width) {
            labelProps.style.width = this.props.width;
        }
        return (
            <label {...labelProps}>
                <select {...this.props} className={styles.select}  value={this.state.value}
                       onChange={e => this.handleChange(e)} />
            </label>
        );
    },

    getInitialState() {
        return {
            value: this.props.value !== undefined ? this.props.value
                : this.props.defaultValue,
        };
    },

    componentWillReceiveProps(props) {
        if (props.value !== undefined) {
            this.setState({value: props.value});
        }
    },

    focus() {
        ReactDOM.findDOMNode(this).querySelector('select').focus();
    },

    handleChange(event) {
        if (this.props.value === undefined) {
            this.setState({value: event.target.value});
        }
        if (this.props.onChange) {
            this.props.onChange(event);
        }
    },
});

module.exports = Select;
