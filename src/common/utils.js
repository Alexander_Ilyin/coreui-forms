/**
 * Created by ilyin on 24.09.2015.
 */
var _ = require("underscore");

var when = function(v) {
    if (v && v.then)
        return v;
    
    return new Promise(
        function (resolve, reject) {
            resolve(v);

        });
}
var id = 10001;
var nextId = function(){
    return id++;
}

var __ = {
    when : when,
    throttle_instance: function (func, timeout) {
        var id = "<throttled>_" + nextId();
        return function () {
            if (!this[id])
                this[id] = _.throttle(func, timeout);
            this[id].apply(this, arguments);
        };
    },

    queue_instance: function (func) {
        var id = "<queued>_" + nextId();

        return function () {
            var args = _.toArray(arguments);
            var self = this;

            function fun() {
                return when(func.apply(self, args));
            }

            return this[id] = when(this[id]).then(fun, fun);
        }
    },

    throttle_deferred_instance: function (func) {
        var id = "<throttled>_" + nextId();
        return function () {
            if (!this[id])
                this[id] = __.throttle_deferred(func);
            return this[id].apply(this, arguments);
        };
    },

    throttle_deferred: function (func) {
        var def = null;
        var nextCallContext = null;
        var nextCall = function () {
            var currentCallContext = nextCallContext;
            nextCallContext = null;
            return when(func.apply(currentCallContext.self, currentCallContext.args));
        }
        var tryEraseDef = function () {
            if (nextCallContext == null)
                def = null;
        }
        return function () {
            if (!def) {
                def = when(func.apply(this, arguments));
                return def;
            }
            var needsToSubscribeDef = (nextCallContext == null);
            nextCallContext = {
                self: this,
                args: arguments
            };
            if (needsToSubscribeDef) {
                def = def
                    .then(nextCall, nextCall)
                    .then(tryEraseDef, tryEraseDef);
            }
            return def;
        };
    }
};
module.exports = __;