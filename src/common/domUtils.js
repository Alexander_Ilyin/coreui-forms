/**
 * Created by ilyin on 12.08.2015.
 */

var ReactDOM = require('react-dom');

module.exports = {

    ie8() {
        var v = this.IEVersion();
        return v && v<=8;
    },

    IEVersion() {
        var myNav = navigator.userAgent.toLowerCase();
        return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : null;
    },

    removeClass : function(el, className) {
        if (!this.hasClass(el, className))
            return;

        if (el.classList)
            el.classList.remove(className);
        else
            el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
    },

    refreshLoop(){
        var self = this;
        if (this.ie8())
            window.setInterval(function(){self.refresh()}, 1000);
    },
    refresh(){
        if (this.ie8()) {
            this.addClass(document.body, "fakeClass");
            this.removeClass(document.body, "fakeClass");
        }
    },

    addClass : function(el, className) {
        if (this.hasClass(el, className))
            return;

        if (el.classList)
            el.classList.add(className);
        else
            el.className += ' ' + className;
    },

     hasClass : function(el, className) {
         if (el.classList)
             return el.classList.contains(className);
         else
             return new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
     },

     ready : function(fn) {
         if (document.readyState != 'loading') {
             fn();
         } else if (document.addEventListener) {
             document.addEventListener('DOMContentLoaded', fn);
         } else {
             document.attachEvent('onreadystatechange', function () {
                 if (document.readyState != 'loading')
                     fn();
             });
         }
     },

    addEventListener : function(el, eventName, handler) {
        if (el.addEventListener) {
            var h = handler;
            el.addEventListener(eventName, handler);

        } else {
            h = function () {handler.apply(el, arguments);};
            el.attachEvent('on' + eventName, h);
        }
        return h;
    },

    removeEventListener : function(el, eventName, handler) {
        if (el.removeEventListener) {
            el.removeEventListener(eventName, handler);

        } else {
            el.detachEvent('on' + eventName, handler);
        }
    },

    outerWidth: function (el, includeMargin) {
        if (!includeMargin)
            return el.offsetWidth;

        var width = el.offsetWidth;
        var style = el.currentStyle || getComputedStyle(el);

        width += parseInt(style.marginLeft) + parseInt(style.marginRight);
        return width;
    },

    outerHeight: function (el, includeMargin) {
        if (!includeMargin)
            return el.offsetHeight;

        var height = el.offsetHeight;
        var style = el.currentStyle || getComputedStyle(el);
        height += parseInt(style.marginTop) + parseInt(style.marginBottom);
        return height;
    },

    getWindowPosition : function(el) {
        return $(el).offset();
        return {
            top: rect.top + document.body.scrollTop,
            left: rect.left + document.body.scrollLeft
        };
    },

    appendReactToBody : function(reactComponent){
        var wrap = document.createElement("div");
        wrap.style.display = 'block';
        wrap.style.position = 'absolute';
        document.body.appendChild(wrap);
        var rendered = ReactDOM.render(reactComponent, wrap);
        return {
            wrap :wrap,
            rendered : rendered
        };
    },


    getScrollTop : function() {
        if (typeof(window.pageYOffset) == 'number') {
            // DOM compliant, IE9+
            return window.pageYOffset;
        }
        else {
            // IE6-8 workaround
            if (document.body && document.body.scrollTop) {
                // IE quirks mode
                return document.body.scrollTop;
            }
            else if (document.documentElement && document.documentElement.scrollTop) {
                // IE6+ standards compliant mode
                return document.documentElement.scrollTop;
            }
        }
        return 0;
    },


    getScrollLeft : function() {
        if (typeof(window.pageXOffset) == 'number') {
            // DOM compliant, IE9+
            return window.pageXOffset;
        }
        else {
            // IE6-8 workaround
            if (document.body && document.body.scrollLeft) {
                // IE quirks mode
                return document.body.scrollLeft;
            }
            else if (document.documentElement && document.documentElement.scrollLeft) {
                // IE6+ standards compliant mode
                return document.documentElement.scrollLeft;
            }
        }
        return 0;
    }
}