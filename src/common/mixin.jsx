var React = require("react");
var Checkbox = require("ui/Checkbox");
var Input = require("ui/Input");
var _ = require("underscore");
var Observable = require("coreui-models/src/observable");
var Tooltip = require("../tooltip");

var ValidationContext = Observable.extend({
    _validationEnabled : false,
    enabled  : function(v){
        if (v===undefined)
            return this._validationEnabled;
        this._validationEnabled = v;
        this.trigger("changed");
    },
    onChanged : function(h,ctx){        this.on("changed",h,ctx);    },
    unChanged : function(h,ctx){        this.un("changed",h,ctx);    }
});



var Mixin = {
    model : function(){
        this.forms();
        if (!this.__prop)
            return this.__model;

        if (!this.__model)
            return null;

        if (this.__model.isSimpleProp(this.__prop))
            return this.__model;
        return this.__model[this.__prop].call(this.__model);
    },
    forms() {
        if (this.__forms)
            return this.__forms;

        if (this.props.form) {
            this.__model = this.props.form.model;
            this.__prop = this.props.form.prop;
            this.__validation = this.props.form.validation;
        }
        else if (this.props.model) {
            this.__model = this.props.model;
            this.__prop = this.props.prop;
            var parent = this.props.parent;

            if (!parent)
                this.__validation = new ValidationContext();
            else {

                this.__validation = parent.__validation;
                if (!this.__validation)
                    throw new Error("Invalid parent! - no validation");
            }
        }
        else
            return;

        if (typeof(this.__prop)=='function')
            this.__prop = this.__prop.propName;

        this.__forms = {
            onChanged : (handler)=> {
                this.changed = this.changed || [];
                this.changed.push(handler);
                this.__model.onChanged(handler);
                this.__validation.onChanged(handler);
            },
            unChanged : (handler) =>{
                this.changed = this.changed || [];
                for (var i = 0; i < this.changed.length; i++)
                {
                    if (this.changed[i] == handler)
                        this.changed.splice(i--, 1);
                }
                this.__model.unChanged(handler);
                this.__validation.unChanged(handler);
            },
            triggerOnChanged : this.__onChanged,
            propName : this.__prop,
            model: this.__model,
            getModelValue: this.__getModelValue,
            setModelValue: this.__setModelValue,
            child: this.__child,
            current: this.__current,
            getActiveError : this.__getActiveError,
            activateValidation: this.__activateValidation,
            activateAllValidation: this.__activateAllValidation,
            deactivateValidation: this.__deactivateValidation,
            deactivateAllValidation: this.__deactivateAllValidation,
            isExpired: this.__isExpired,
            getErrorTooltip: (cfg)=> {
                this.__findTooltipTarget = cfg.findTarget;
                return {
                    update: this.__updateErrorTooltip
                }
            }
        }
        return this.__forms;
    },

    componentWillMount: function() {
        var m = this.model();
        if (!m)
            return;
        this.model().onChanged(this.__onModelChanged, this);
    },

    componentWillUnmount: function() {
        var model = this.model();
        if (!model)
            return;
        if (this.__prop && this.__model.isSimpleProp(this.__prop))
            model.clearError(this.__prop, Mixin.InvalidFormatCode);
        if (this.changed)
            for (var i = 0; i < this.changed.length; i++) {
                model.unChanged(this.changed[i]);
                this.__validation.unChanged(this.changed[i]);
            }
        if (this.tooltip)
            this.tooltip.dispose();
        this.model().unChanged(this.__onModelChanged, this);
    },

    __onChanged(){
        if (!this.changed)
            return;
        for (var i = 0; i < this.changed.length; i++)
            this.changed[i].call(this);
    },

    __isExpired(){
        return !(this.renderedVersion>=this.__model.getVersion());
    },

    __onModelChanged()
    {
        if (!this.__isExpired())
            return;
        this.setState({});
    },

    componentWillUpdate()
    {
        if (this.__model)
            this.renderedVersion = this.__model.getVersion();
    },

    __activateValidation : function(){
        if (this.validate)
            return;

        this.validate = true;
        this.__onChanged();
    },

    __activateAllValidation : function(){
        this.__validation.enabled(true);
    },

    __deactivateValidation: function(){
        if (!this.validate)
            return;

        this.validate = false;
        this.__onChange();
    },

    __deactivateAllValidation : function(){
        this.__validation.enabled(false);
    },

    __child : function(){
        if(typeof(arguments[0])=="string") {
            var prop = arguments[0];
            return {
                model : this.model(),
                validation : this.__validation,
                prop : prop,

                deactivateValidation: function() {
                    this.validation.enabled(false);
                }
            };
        }
        else {
            return {
                model : arguments[0],
                prop : arguments[1],
                validation : this.__validation,

                deactivateValidation: function() {
                    this.validation.enabled(false);
                }
            };
        }
    },

    __current : function(){
        return {
            model : this.__model,
            prop: this.__prop,
            validation : this.__validation,

            deactivateValidation: function() {
                this.validation.enabled(false);
            }
        };
    },

    // visible: bool, error:error
    __updateErrorTooltip : function(cfg) {
        this.tooltipState = _.extend({}, this.tooltipState, cfg);
        if (!this.tooltipState || !this.tooltipState.error || !this.tooltipState.visible) {
            if (this.tooltip)
                this.tooltip.hide();
            return;
        }
        var err = this.tooltipState.error;
        if (this.tooltipState.visible && err) {
            var message = err.msg;
            if (!this.tooltip)
                this.tooltip = Tooltip.show(
                    <ErrorMessage message={message}/>,
                    {
                        pos: this.props.tooltipPos || "right-top",
                        target: this.__findTooltipTarget
                    });
            else {
                this.tooltip.content.setState({message: message})
            }
            this.tooltip.show();
        }
    },

    __getActiveError : function(){
        var hasGetter = this.__prop && this.__model.hasGetter(this.__prop);
        var isAuto = hasGetter && this.__model.getterEnabled(this.__prop);
        var validationIsActive =  this.validate || this.__validation.enabled();
        if (validationIsActive || isAuto) {
            if (this.__prop) {
                var errors = this.__model.getErrors(this.__prop);
                return this.__model.getError(this.__prop);
            }
            return null;
        }
        else
            return null;
    },

    __setModelValue : function(v) {
        if (this.__prop)
            this.__model[this.__prop](v);
        else
            this.__model.update(v);
    },

    __getModelValue : function(){
        if (this.__prop)
            return this.__model[this.__prop]();
        else
            return this.__model.toJson();
    }

};

Mixin.InvalidFormatCode = "BadFormat";

var ErrorMessage = React.createClass({
    getInitialState(){
    return {message : this.props.message};
},

render : function(){
    return <div style={{ display:"block", padding: "4px 14px", lineHeight: "20px "}} dangerouslySetInnerHTML={{__html:this.state.message}}></div>;
}
});


module.exports = Mixin;