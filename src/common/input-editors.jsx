var React = require("react");
var ReactDOM = require("react-dom");
var mixin = require("./mixin.jsx");
var Input = require("retail-ui/components/Input");
var Group = require("retail-ui/components/Group");
var Button = require("retail-ui/components/Button");
var Icon = require("retail-ui/components/Icon");
var Base = require("coreui-models/src/base");
var _ = require("underscore");

var InputBasedEditor = {
    mixins: [mixin],
    getInitialState(){ return {}},

    componentDidMount() {
        this.forms().onChanged(this._importStateFromModel);
        this._importStateFromModel();
    },

    render() {
        var inputProps = _.extend({}, this.props);
        delete inputProps.width;
        var width = this.props.width || 250;
        var error  = this.state.error;

        var icon = this.state.isAuto ? <Icon name="fx"/> : null;
        var title = error != null ? error.msg : this.props.title;
        var input = <Input
                           leftIcon={icon}
                           type='text'
                           key="editor"
                           ref="input"
                           mainInGroup
                           onFocus={this._onFocus}
                           onBlur={this._onBlur}
                           onChange={this._onChange}
                           value={this.state.value}
                           title={title}
                           error={error!=null}
                           {...inputProps}/>;

        if (this.state.isAuto || !this.state.hasGetter)
            return (<div style={{display:'inline-block'}}><Group width={width}>{input}</Group></div>);

        var button = (<Button narrow onClick={this._onRestore}><Icon name="undo" /></Button>);
        return (<div style={{display:'inline-block'}}><Group width={width}>{button}{input}</Group></div>);
    },


    _onFocus : function(){ this._getErrorTooltip().update({visible:true});},
    _onBlur : function(){
        this.forms().activateValidation();
        this._getErrorTooltip().update({visible:false});
    },
    _getErrorTooltip : function(){
        return this.forms().getErrorTooltip({
            findTarget : ()=>this.refs.input ? ReactDOM.findDOMNode(this.refs.input) : null
        });
    },

    _onRestore : function(){
        var model = this.forms().model;
        var propName = this.forms().propName;
        model.getterEnabled(propName, true);
    },

    _onChange: function (e) {

        var propName = this.forms().propName;
        var model = this.forms().model;
        var value = e.target.value;
        var convertedValue = {result : value};
        if (this.convertValueToModel)
            convertedValue = this.convertValueToModel(value);

        var convertError = convertedValue.error;
        if (convertedValue && convertError) {
            model.setError(propName, convertError);
            this.forms().setModelValue(null);
            this.forms().activateValidation();
            this.setState({
                value : value,
                error : convertError
            });
            this._getErrorTooltip().update({error: convertError});
        }
        else {
            this.forms().setModelValue(convertedValue.result);
            var error = this.forms().getActiveError();
            this.setState({
                value : value,
                error : error
            });
        }
    },

    _importStateFromModel : function() {

        var error = this.forms().getActiveError();
        var propName = this.forms().propName;
        var model = this.forms().model;
        var hasGetter = model.hasGetter(propName);

        var value = this.forms().getModelValue();
        if (this.convertModelToValue)
            value = this.convertModelToValue(value);

        this.setState({
            value : value,
            error : error,
            hasGetter : hasGetter,
            isAuto : hasGetter && model.getterEnabled(propName)
        });
        this._getErrorTooltip().update({error:error});
    }
};



var TextEditor = React.createClass(_.extend({},
    InputBasedEditor, {}
));

var FloatEditor = React.createClass(_.extend({}, InputBasedEditor, {
    convertValueToModel : function(value) {
        if (value=="")
            return {result : null};
        value = value.replace(",",".");
        if (/^\d+(\.\d*)?$/.test(value)){
            value = parseFloat(value);
            if (!isNaN(value))
                return {
                    result : value,
                };
        }
        return {
            error : {
                msg : "Неверный формат",
                code : mixin.InvalidFormatCode
            }
        };
    },

    convertModelToValue : function(value){
        if (value==null)
            return "";
        value = Math.round(value*10000000)/10000000;
        return value.toString();
    }
}));

var IntEditor = React.createClass(_.extend({}, InputBasedEditor, {
    convertValueToModel : function(value) {
        if (value == "")
            return {result: ""};
        if (/^\d+?$/.test(value)) {
            value = parseFloat(value);
            if (!isNaN(value))
                return {
                    result: value,
                };
        }
        return {
            error: {
                msg: "Неверный формат",
                code: mixin.InvalidFormatCode
            }
        }

    },
    convertModelToValue : function(value){
        if (value==null)
            return "";
        return value.toString();
    }
}));


module.exports = {
    TextEditor : TextEditor,
    NumberEditor : FloatEditor,
    NumEditor : FloatEditor,
    IntEditor : IntEditor
};
