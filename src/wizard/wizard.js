/**
 * Created by ilyin on 27.08.2015.
 */

const styles = require('./wizard.css');

var React = require("react");
var ReactDOM = require("react-dom");
var ReactCSSTransitionGroup = require('react-addons-css-transition-group');
var cloneWithProps = require('react-addons-clone-with-props');

var contentId = 0;

var Wizard = React.createClass({
    render(){
        return (<div ref='container' style={{display:"inline-block"}} ></div>);
    },

    componentDidMount(){
        if (this.start)
            this.start();
        else
            throw "Error! start method is missing!";
    },

    show : function(content){
        contentId++;
        content = React.cloneElement(content, {
            cancel : ()=>this.cancel(),
            close : ()=>this.close()
        });
        ReactDOM.unmountComponentAtNode(ReactDOM.findDOMNode(this.refs.container));
        var newContent = ReactDOM.render(
            //<ReactCSSTransitionGroup transitionName="example" transitionAppear={true}>
                <div key={contentId}>
                  {content}
                </div>
            //</ReactCSSTransitionGroup>
            ,ReactDOM.findDOMNode(this.refs.container));
    },

    close() {
        if (this.props.close)
            this.props.close();
    },

    cancel() {
        if (this.props.cancel)
            this.props.cancel();
    }
})



module.exports = Wizard;