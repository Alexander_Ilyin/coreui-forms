const React = require('react');

const PropTypes = React.PropTypes;

import styles from './Box.less';

var TooltipBox = React.createClass({
  pos : 'top',

  getInitialState : function(){
     return {
        offset : this.props.offset || 10,
        pos : this.props.pos || 'top'
     }
  },

  render() {
    var pinSize = 8;
    var offset = this.state.offset;

    var box = {
      position : 'relative'
    };
    var pin = {
      position : 'absolute',
      border:  pinSize + 'px solid #DADADA',
    };
    var pinInner = {
      position : 'absolute',
      border: pinSize + 'px solid #FFF',
    };

    var settingsList = {
      left : { hiddenBorders : [0,2,3], position:["+offset",null,null,-1], zeroBorder: "borderLeft"},
      top : { hiddenBorders : [0,1,3], position:[-1,null,null,"+offset"], zeroBorder: "borderTop"},
      right : { hiddenBorders : [0,1,2], position:["+offset",-1,null,null], zeroBorder: "borderRight"},
      bottom : { hiddenBorders : [1,2,3], position:[null,null,-1,"+offset"], zeroBorder: "borderBottom"}
    };

    var pinSettings = settingsList[this.state.pos];

    var borderNames = ['borderTopColor','borderRightColor','borderBottomColor','borderLeftColor'];
    var positions = ['top','right','bottom','left'];
    for (var i = 0; i < borderNames.length; i++) {
      var border = borderNames[i];
      pin[border] = '#DADADA';
      pinInner[border] = '#FFF';
    }

    for (var i = 0; i < pinSettings.hiddenBorders.length; i++) {
      var border = borderNames[pinSettings.hiddenBorders[i]];
      pin[border] = 'transparent';
      pinInner[border] = 'transparent';
    }

    for (var i = 0; i < positions.length; i++) {
      var p = pinSettings.position[i];
        if (p===null)
        continue;

      if (p=="+offset")
        p = offset;
      else if (p=="-offset")
        p = -offset;
      else {
        p = p * 2 * pinSize;
      }

      /*
      +0 -0 ��������� ���� IE8. ����� ������ �� ��������� IE8 �� css ����� border � +0 -0 ������ +1 -1, � +1 -1 ������,
      ��������������, +2 -2
      */
      var pinVal = p;
      var pinInnerVal = p;
      if (pinSettings.position[i] == 1) {
        pinVal -= 0;
        pinInnerVal -= 1;
      }
      else if (pinSettings.position[i] == -1) {
        pinVal += 0;
        pinInnerVal += 1;
      }

      var pos = positions[i];
      pin[pos] = pinVal;
      pinInner[pos] = pinInnerVal;
    }
    var container = {};


    return (
        <div style={box}>
          <div className={styles.inner}>
            <div style={pin}></div>
            <div style={pinInner}></div>
            <div ref='childrenContainer' style={container} className="ui-TooltipContainer">
              {this.props.children}
            </div>
          </div>
        </div>);
  }
});

module.exports = TooltipBox;
