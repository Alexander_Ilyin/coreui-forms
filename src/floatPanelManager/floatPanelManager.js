/**
 * Created by ilyin on 11.08.2015.
 */

var React = require('react');
var ReactDOM = require('react-dom');
var _ = require("underscore");
var DomUtils = require("./domUtils");
var appendReactToBody = DomUtils.appendReactToBody;


//cfg : render, position
var Panel = function(cfg){
    _.extend(this, {
        render: function(){return (<div>Render func is missing!</div>)},
        align : 'middle-left'
    },cfg);
};

var panels = [];
function updatePanels(){
    _.each(panels, function(p){
        p.updatePosition();
    });
};

DomUtils.addEventListener(window, 'scroll', updatePanels);
DomUtils.addEventListener(window, 'resize', updatePanels);
DomUtils.ready(updatePanels);

var zIndex = 1000;
var genZIndex = function(){
    return ++zIndex;
};

Panel.prototype = {
    init : function(){
        if (!this.contentWrap) {
            var appendReactToBodyResult = appendReactToBody(this.render());
            this.contentWrap = appendReactToBodyResult.wrap;
            this.content = appendReactToBodyResult.rendered;
        }
        if (!this.panelAppendResult) {
            var layoutCfg = this.getLayout();
            this.position = this.getPosition(layoutCfg);
            var reactPanel = this.renderPanel()
            this.contentWrap.style.visibility = '';
            this.contentWrap.style.position = '';

            this.panelAppendResult = DomUtils.appendReactToBody(reactPanel);
            ReactDOM.findDOMNode(this.panelAppendResult.rendered.refs.childrenContainer).appendChild(this.contentWrap);
            this.panelAppendResult.wrap.style.visibility = 'hidden';
            this.panelAppendResult.wrap.style.zIndex = genZIndex();
        }
    },

    show : function(){
        if (this.visible) {
            return;
        }
        this.visible = true;
        this.panelAppendResult.wrap.style.visibility = '';
        this.animateAppear(this.panelAppendResult.wrap, this.position);
        updatePanels();
    },

    updatePosition : function(){
        if (!this.visible)
            return;

        var layoutCfg = this.getLayout();
        this.position = this.getPosition(layoutCfg);
        this.panelAppendResult.wrap.style.left = this.position.left + 'px';
        this.panelAppendResult.wrap.style.top = this.position.top + 'px';
        this.updatePanel(this.panelAppendResult.rendered);
    },

    getLayout : function(){
        var target = this.target;
        if (typeof(target)=="function")
            target = target();

        var topLeft = DomUtils.getWindowPosition(target);
        return {
            targetRect : {
                top : topLeft.top,
                    left : topLeft.left,
                    width : DomUtils.outerWidth(target),
                    height : DomUtils.outerHeight(target)
            },
            contentSize : {
                width : DomUtils.outerWidth(this.contentWrap),
                height : DomUtils.outerHeight(this.contentWrap)
            }
        };
    },

    updatePanel : function(panel){
    },

    hide : function(){
        if (!this.visible)
            return;
        this.visible = false;
        this.animateDisappear();
    },

    animateDisappear : function(panelWrap) {
        this.panelAppendResult.wrap.style.display = 'none';
    },

    animateAppear : function(panelWrap, position) {
        panelWrap.style.visibility = '';
        panelWrap.style.display = '';
        panelWrap.style.left = position.left + 'px';
        panelWrap.style.top = position.top + 'px';
    },

    renderPanel : function() {
        return <SimplePanel/>;
    },

    getPosition : function(cfg){
        return {
            left: cfg.targetRect.left,
            top : cfg.targetRect.top - cfg.contentSize.height
        };
    },

    dispose : function(){
        ReactDOM.unmountComponentAtNode(this.contentWrap);
        var panelDom = this.panelAppendResult.wrap;
        ReactDOM.unmountComponentAtNode(panelDom);
        panelDom.parentNode.removeChild(panelDom);
        panels = _.reject(panels, (x)=>{ return x==this});
    }
};

var SimplePanel = React.createClass({
    render : function(){
        return <div ref='childrenContainer'></div>;
    }
});

var createPanel = function(cfg){
    var panel = new Panel(cfg);
    panels.push(panel);
    panel.init();
    return panel;
};


module.exports = {
    createPanel : createPanel,
    updatePanels : updatePanels,
    genZIndex : genZIndex
};