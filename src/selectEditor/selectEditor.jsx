var React = require("react");
var forms = require("../common/mixin.jsx");
var Input = require("retail-ui/components/Input");
var DatePicker = require("retail-ui/components/DatePicker");
var Group = require("retail-ui/components/Group");
var Button = require("retail-ui/components/Button");
var Icon = require("retail-ui/components/Icon");
var Autocomplete = require("../Autocomplete");
var AutocompleteEditor = require("../AutocompleteEditor");
var Base = require("coreui-models/src/base");
var _ = require("underscore");
var log = function(a){return console.log(a);};

var SelectEditor = React.createClass({
    render() {
        return <AutocompleteEditor  {...this.props} mode="input-select"/>;
    }
});

module.exports = SelectEditor;