var React = require("react");
var _ = require("underscore");
var TabSelect = require("../tabSelect").TabSelect;
var TabSelectItem = require("../tabSelect").TabSelectItem;
var forms = require("../common/mixin.jsx");
var cloneWithProps = require('react-addons-clone-with-props');

var TabSelectEditor = React.createClass({
    mixins : [forms],
    render() {
        var children = this.props.children.map(child=>{
            var val = child.props.value;
            return React.cloneElement(child, {
                selected : val==this.forms().getModelValue(),
                onClick : ()=>this._onSelect(val)
            });
        })
        var t = <TabSelect {...this.props} children={children}/>;
        return t;
    },

    _onSelect(val){
        this.forms().setModelValue(val);
    }
});

module.exports = TabSelectEditor;