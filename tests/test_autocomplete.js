var _ = require("underscore");
var $ = require("jquery");
var assert = require("assert");
var m = require("coreui-models");

var React = require("react");
var ReactDOM = require("react-dom");
var TextEditor = require("coreui-forms/src/textEditor");
var AutocompleteEditor = require("coreui-forms/src/autocompleteEditor");
var NumEditor = require("coreui-forms/src/numEditor");
var Mixin = require("coreui-forms/src//mixin");

var Icon = require("retail-ui/components/Icon");
var Box = require("retail-ui/components/Tooltip/Box");
var TooltipManager = require("coreui-forms/src/Tooltip");
var FlowManager = require("coreui-forms/src/floatPanelManager");
var TestUtils = require("react-addons-test-utils");
var TestHelper = require("./testHelper");

var _ = require("underscore");

var getTestDomElement = require("./getTestDomElement");


var Streets = [
    {streetId:"S1", name : "Lenina"},
    {streetId:"S2", name : "Lunacharskogo"},
];

var Address = m.define({
    streetId : m.field().required(),
    house : m.field().required(),
});

var getStreets = function(query){ return $.when(_.filter(Streets, (x)=>x.name.indexOf(query || "")==0));    };
var findStreet = function(value){         return $.when(_.filter(Streets, (x)=>x.streetId==value)[0]);    };


test("ac1. Check initial value loaded to model ", function(){


    var AddressForm = React.createClass({
        mixins: [Mixin],
        render() {
            return (<div>
                Street :                <br/>
                <AutocompleteEditor
                    form={this.forms().child('streetId')}
                    displayField="name"
                    valueField="streetId"
                    source={getStreets}
                    findItem={findStreet}
                    width="100"/>
                <br/>
                SelectedStreetId : {this.model().streetId()} <br/>
            </div>);
        }
    });

    var c = getTestDomElement();
    ReactDOM.render((<AddressForm model={new Address({streetId:"S2", house:"28"})}/>),c);
    assert.equal($(c).find("input").val(), "Lunacharskogo");
});

test("ac2. Update model, check input value updated ", function(){


    var AddressForm = React.createClass({
        mixins: [Mixin],
        render() {
            return (<div>
                Street :                <br/>
                <AutocompleteEditor
                    form={this.forms().child('streetId')}
                    displayField="name"
                    valueField="streetId"
                    source={getStreets}
                    findItem={findStreet}
                    width="100"/>
                <br/>
                SelectedStreetId : {this.model().streetId()} <br/>
            </div>);
        }
    });

    var c = getTestDomElement();
    var model = new Address({streetId:"S2", house:"28"});
    ReactDOM.render((<AddressForm model={model}/>),c);

    model.streetId("S1");
    assert.equal($(c).find("input").val(), "Lenina");

    model.streetId("S2");
    assert.equal($(c).find("input").val(), "Lunacharskogo");

});




test("ac3. Set value to null, check input value updated ", function(){
    var AddressForm = React.createClass({
        mixins: [Mixin],
        render() {
            return (<div>
                Street :                <br/>
                <AutocompleteEditor
                    form={this.forms().child('streetId')}
                    displayField="name"
                    valueField="streetId"
                    source={getStreets}
                    findItem={findStreet}
                    width="100"/>
                <br/>
                SelectedStreetId : {this.model().streetId()} <br/>
            </div>);
        }
    });

    var c = getTestDomElement();
    var model = new Address({streetId:"S2", house:"28"});
    ReactDOM.render((<AddressForm model={model}/>),c);

    model.streetId("S1");
    assert.equal($(c).find("input").val(), "Lenina");

    model.streetId(null);
    assert.equal($(c).find("input").val(), "");

});

test("ac4. Update model, check Re-Render does not clear input value ", function(){


    var AddressForm = React.createClass({
        mixins: [Mixin],
        render() {
            return (<div>
                Street :                <br/>
                <AutocompleteEditor
                    form={this.forms().child('streetId')}
                    displayField="name"
                    valueField="streetId"
                    source={getStreets}
                    findItem={findStreet}
                    width="100"/>
                <br/>
                SelectedStreetId : {this.model().streetId()} <br/>
            </div>);
        }
    });

    var c = getTestDomElement();
    var model = new Address({streetId:"S2", house:"28"});
    var form = ReactDOM.render((<AddressForm model={model}/>),c);
    model.streetId("S1");
    assert.equal($(c).find("input").val(), "Lenina");
    form.setState({foo:"BAR"});
    assert.equal($(c).find("input").val(), "Lenina");
});


test("ac5. Enter invalid input value, check model is null ", function(cb){
    var AddressForm = React.createClass({
        mixins: [Mixin],
        render() {
            return (<div>
                Street :                <br/>
                <AutocompleteEditor
                    form={this.forms().child('streetId')}
                    displayField="name" valueField="streetId"
                    source={getStreets} findItem={findStreet}
                    width="100"/>
                <br/>
                SelectedStreetId : {this.model().streetId()} <br/>
            </div>);
        }
    });

    var c = getTestDomElement();
    var model = new Address({streetId:"", house:"28"});
    var form = ReactDOM.render((<AddressForm model={model}/>),c);
    model.streetId("S1");
    assert.equal($(c).find("input").val(), "Lenina");
    new TestHelper(c)
        .type("input",'Hello kitty!')
        .check("input", $el=>$el.val()=='Hello kitty!')
        .check("input", $el=>{assert.equal(model.streetId(), null)})
        .go(cb);
});

test("ac6. Enter invalid input value, check error is shown", function(){
    var AddressForm = React.createClass({
        mixins: [Mixin],
        render() {
            return (<div>
                Street :                <br/>
                <AutocompleteEditor
                    form={this.forms().child('streetId')}
                    displayField="name" valueField="streetId"
                    source={getStreets} findItem={findStreet}
                    width="100"/>
                <br/> 
                SelectedStreetId : {this.model().streetId()} <br/>
            </div>);
        }
    });

    var c = getTestDomElement();
    var model = new Address({streetId:"", house:"28"});
    var form = ReactDOM.render((<AddressForm model={model}/>),c);
    model.streetId("");
});