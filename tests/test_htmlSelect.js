var $ = require("jquery");
var assert = require("assert");
var ReactDOM = require("react-dom");
var React = require("react");
var TextEditor = require("../src/textEditor");
var CheckboxEditor = require("../src/checkboxEditor");
var DateEditor = require("../src/dateEditor");
var NumEditor = require("../src/numEditor");
var HtmlSelect = require("../src/htmlSelect");
var HtmlSelectEditor = require("../src/htmlSelectEditor");
var SelectEditor = require("../src/SelectEditor");
var forms = require("../src/common/mixin.jsx");

var Mixin = require("../src/mixin");
var m = require("coreui-models");
var Icon = require("retail-ui/components/Icon");
var Tooltip = require("coreui-forms/src/tooltip");
var FlowManager = require("coreui-forms/src/floatPanelManager");
var getTestDomElement = require("./getTestDomElement");

var SampleModel = m.define({
    type : m.field()
});


test("hs-03. test html SelectEditor ", function(){
    var log = "";
    var model = new SampleModel();
    var TestForm = React.createClass({
        mixins: [forms],
        render() {
            return (<div>
                Selected model value : {this.model().type()}<br/>
                <SelectEditor form={this.forms().child('type')} source={()=>$.when([
                    {orgId:"O1", orgTitle :"Org1"},
                    {orgId:"O2", orgTitle :"Org2"}
                ])} displayField='orgTitle' valueField="orgId">
                </SelectEditor>
            </div>);
        }
    });

    var c = getTestDomElement();
    ReactDOM.render((<TestForm model={model}/>),c);
});


test("hs-02. test html HtmlSelectEditor ", function(){
    var log = "";
    var model = new SampleModel();
    var TestForm = React.createClass({
        mixins: [forms],
        render() {
            return (<div>
                Selected model value : {this.model().type()}
                <HtmlSelectEditor form={this.forms().child('type')}>
                    <option value="">Любой</option>
                    <option value="Invoice">СФ</option>
                    <optgroup label="Счет-фактура">
                        <option value="Invoice3" >Любой счет-фактура</option>
                        <option value="Invoice2">Исходный СФ</option>
                    </optgroup>
                </HtmlSelectEditor>
            </div>);
        }
    });

    var c = getTestDomElement();
    ReactDOM.render((<TestForm model={model}/>),c);
});


test("hs-01. test html select ", function(){
    var log = "";
    var TestForm = React.createClass({
        render() {
            return (<div>
                Selected : {(this.state && this.state.selected ) + ""}
                <HtmlSelect value='Invoice2' onChange={(e)=>{this.setState({selected : e.target.value});console.log(e);}}>
                    <option value="Invoice">СФ</option>
                    <optgroup label="Счет-фактура">
                        <option value="Invoice3" >Любой счет-фактура</option>
                        <option value="Invoice2">Исходный СФ</option>
                    </optgroup>
                 </HtmlSelect>
            </div>);
        }
    });

    var c = getTestDomElement();
    ReactDOM.render((<TestForm />),c);

//    assert.equal($(c).find("input").val(), "Lunacharskogo");
});

