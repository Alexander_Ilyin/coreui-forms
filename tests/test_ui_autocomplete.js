/**
 * Created by ilyin on 21.08.2015.
 */
var ReactDOM = require("react-dom");
var TestUtils = require('react-addons-test-utils');
var TestHelper =require("../tests/testHelper");
var Autocomplete = require("../src/autocomplete");
var React = require("react");
var assert = require("assert");
var _ = require("underscore");
var $ = require("jQuery");

var getTestDomElement = require("./getTestDomElement");


var source = [
    {name:"Russia", code:"C1"},
    {name:"Romania", code:"C2"},
    {name:"Usa", code:"C3"},
    {name:"Canada", code:"C4"}
];
var items = (query, code)=> {
    var res = _.filter(source, x=>{return x.name.indexOf(query)>=0});
    return $.when(res);
};


test("ui-01. select item, check onChange triggered", function(cb){
    var lastText, lastItem;
    var onChange = function(text, item) {         lastText = text; lastItem = item;};

    var c = getTestDomElement();
    var rendered = ReactDOM.render((<Autocomplete source={items} displayField="name" onChange={onChange}/>),c);
    var $input = $(c).find("input");
    $input.focus();

    TestUtils.Simulate.change($input[0], {target: {value: 'Russ'}});
    setTimeout(()=>{
        assert.equal(lastText, "Russ");
        assert.equal(lastItem, null);

        TestUtils.Simulate.keyDown($input[0], {key: "Enter", keyCode: 13, which: 13});

        setTimeout(()=>{
            TestUtils.Simulate.keyDown($input[0], {key: "Enter", keyCode: 13, which: 13});
            assert.equal(lastText, "Russia");
            assert.deepEqual(lastItem, {name:"Russia", code:"C1"});
        }, 100);

        cb();
    }, 100);


});


test("ui-02. show list, check arrowDown selects next item", function(){
    var lastText, lastItem;
    var onChange = function(text, item) {         lastText = text; lastItem = item;};

    var c = getTestDomElement();
    var rendered = ReactDOM.render((<Autocomplete source={items} displayField="name" onChange={onChange}/>),c);
    var $input = $(c).find("input");
    $input.focus();
    TestUtils.Simulate.keyDown($input[0], {key: "ArrowDown"});
    TestUtils.Simulate.keyDown($input[0], {key: "Enter", keyCode: 13, which: 13});
    assert.equal(lastText, "Romania");
    assert.deepEqual(lastItem, {name:"Romania", code:"C2"});
});



test("ui-03. show list, enter text value, check item is selected automatically", function(cb){
    var lastText, lastItem;
    var onChange = function(text, item) {         lastText = text; lastItem = item;};

    var c = getTestDomElement();
    var rendered = ReactDOM.render((<Autocomplete source={items} displayField="name" onChange={onChange}/>),c);
    var $input = $(c).find("input");
    $input.focus();

    var helper = new TestHelper(c);
    helper.type($input[0], 'Romania')
        .blur($input[0])
        .check($input[0], ()=>{
            return lastText=="Romania" && _.isEqual(lastItem, {name:"Romania", code:"C2"});
        }).go(cb);
});

test("ui-04. show list, drop component, check menu missing ", function(){
    assert.equal($(".ui-AutocompleteMenu:visible").length, 0);

    var lastText, lastItem;
    var onChange = function(text, item) {         lastText = text; lastItem = item;};

    var c = getTestDomElement();
    var rendered = ReactDOM.render((<Autocomplete source={items} displayField="name" onChange={onChange}/>),c);
    var $input = $(c).find("input");
    $input.focus();
    TestUtils.Simulate.focus($input[0]);
    assert.equal($(".ui-AutocompleteMenu:visible").length, 1);

    var rendered = ReactDOM.render((<div/>),c);
   assert.equal($(".ui-AutocompleteMenu:visible").length, 0);

});


test("ui-05. input text, check text goes to query", function(cb){
    var lastQuery;
    var getItems = function(query) { lastQuery= query; return items(query);};

    var c = getTestDomElement();
    var rendered = ReactDOM.render((<Autocomplete source={getItems} displayField="name" />),c);
    var $input = $(c).find("input");

    var helper = new TestHelper(c);
    helper
        .focus($input)
        .type($input, 'Alalalal')
        .check($input, ()=>assert.equal(lastQuery, 'Alalalal'))
        .go(cb);
});

test("ui-06. test flaots", function(){
    var Panel = React.createClass({
         render(){
             return <div><div>{this.props.children}</div>{new Date().getMilliseconds()}</div>;
         }
    });

    var Owner =  React.createClass({
        getInitialState(){
            return {header: "324"};
        },
        render(){
            return <div onClick={this.onClick}>MAIN {this.state.header}</div>;
        },
        onClick : function(){
           this.setState({header: + new Date().getMilliseconds()+""});
            this.panel.setState({header:"sadasd"});
        },
        componentDidMount() {
            var c3 = getTestDomElement(3);
            this.panel = ReactDOM.render(<Panel><h1> {new Date().getMilliseconds()}</h1></Panel>, c3);
        }
    });
    var c2 = getTestDomElement(2);
    var rendered = ReactDOM.render((<div> <Owner ></Owner></div>),c2);
    rendered.setState({header:"23423443"});
});

