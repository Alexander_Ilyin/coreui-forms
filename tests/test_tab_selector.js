var $ = require("jquery");
var ReactDOM = require("react-dom");
var assert = require("assert");
var React = require("react");
var TabSelect = require("../src/tabSelect").TabSelect;
var TabSelectEditor = require("../src/tabSelectEditor");
var TabSelectItem = require("../src/tabSelect").TabSelectItem;
var _ = require("underscore");
var TestUtils = require("react-addons-test-utils");
var TestHelper = require("./testHelper");
var getTestDomElement = require("./getTestDomElement");
var Mixin = require("coreui-forms/src//mixin");
var m = require("coreui-models");

test("te-1.", function(){

    var SampleForm = React.createClass({
        render: function () { return (<div >Value :

            <TabSelect width="500px">
                <TabSelectItem text='I квартал' />
                <TabSelectItem text='Полугодие'  selected={true}/>
                <TabSelectItem text='9 месяцев' />
                <TabSelectItem text='год' value='12months'/>
            </TabSelect>
        </div>);}
    });

    var c = getTestDomElement();
    var rendered = ReactDOM.render((<SampleForm />),c);


});
test("te-2.", function(cb){

    var Model = m.define({
        period : m.field()
    })
    var model = new Model({period:"9months"});

    var SampleForm = React.createClass({
        render: function () { return (<div >Value :

            <TabSelectEditor width="500px" model={model} prop="period">
                <TabSelectItem text='I квартал' value='3months' role='3months' />
                <TabSelectItem text='Полугодие' value='6months' selected={true} role='6months' />
                <TabSelectItem text='9 месяцев' value='9months' role='9months' />
                <TabSelectItem text='год' value='12months' role='12months' />
            </TabSelectEditor>
        </div>);}
    });


    var c = getTestDomElement();
    var rendered = ReactDOM.render((<SampleForm />),c);
    var t = new TestHelper(c);
    t.check("~9months", $el=>$el.attr("data-x-selected")=='true')
    .click("~3months")
    .check("~3months", $el=>$el.attr("data-x-selected")=='true')
    .go(cb);

});