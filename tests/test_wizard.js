/**
 * Created by ilyin on 27.08.2015.
 */

var Wizard = require("../src/wizard/wizard.js");
var Lightbox = require("../src/Lightbox");
var Loading = require("../src/Lightbox/Loading");
var Button = require("retail-ui/components/Button");
var TestUtils = require('react-addons-test-utils');
var assert = require("assert");
var React= require("react");

var testEl = require("./getTestDomElement");


test("wt6. test wizard cancel rejects defered", function(cb){
    class TestLoadingWizard extends Wizard {
        start() {
            this.show(<Lightbox width='400px' closable='true'>
                <Lightbox.Header>Wizz for </Lightbox.Header>
                <Lightbox.Body > some text some text some text some text </Lightbox.Body>
                <Lightbox.Footer>
                    <Button>Show nested</Button>
                    <Button onClick={()=>this.go()}>Show nested2 </Button>
                </Lightbox.Footer>
            </Lightbox>);
        }
    };
    var lb = Lightbox.show(<TestLoadingWizard/>);
    TestUtils.Simulate.click($('.lightbox-close')[0]);
    lb.result.catch(function(x){cb();    })
});

test("wt5. test wizard close resolves defered", function(cb){
    class TestLoadingWizard extends Wizard {
        start() {
            this.show(<Lightbox width='400px' closable='true'>
                <Lightbox.Header>Wizz for </Lightbox.Header>
                <Lightbox.Body > some text some text some text some text </Lightbox.Body>
                <Lightbox.Footer>
                    <Button>Show nested</Button>
                    <Button onClick={()=>this.go()}>Show nested2 </Button>
                </Lightbox.Footer>
            </Lightbox>);
        }
    };
    var lb = Lightbox.show(<TestLoadingWizard/>);
    lb.close(":)");
    lb.result.done(function(x){
        assert.equal(x, ":)");
        cb();
    })
});


test("wt4. test wizard can be closed", function(){
    class TestLoadingWizard extends Wizard {
        start() {
            this.show(<Lightbox width='400px' closable='true'>
                <Lightbox.Header>Wizz for </Lightbox.Header>
                <Lightbox.Body > some text some text some text some text </Lightbox.Body>
                <Lightbox.Footer>
                    <Button>Show nested</Button>
                    <Button onClick={()=>this.go()}>Show nested2 </Button>
                </Lightbox.Footer>
            </Lightbox>);
        }
    };
    Lightbox.show(<TestLoadingWizard/>);
    assert.equal($('.lightbox-container:visible').length, 1);
    TestUtils.Simulate.click($('.lightbox-close')[0]);
    assert.equal($('.lightbox-container:visible').length, 0);
});

test("wt3. test lightbox can be closed", function(){
    Lightbox.show(
        <Lightbox width='400px' closable='true'>
        <Lightbox.Header>Header for </Lightbox.Header>
        <Lightbox.Body > some text some text some text some text </Lightbox.Body>
        <Lightbox.Footer>
            <Button>Show nested</Button>
            <Button onClick={()=>this.go()}>Show nested2 </Button>
        </Lightbox.Footer>
    </Lightbox>);
    assert.equal($('.lightbox-container:visible').length, 1);
    TestUtils.Simulate.click($('.lightbox-close')[0]);
    assert.equal($('.lightbox-container:visible').length, 0);
});

test("wt1. test loading wizard", function(){
    return;
    class TestLoadingWizard extends Wizard {
        start() {
            this.show(<Loading>Идёт загрузка</Loading>);
        }
    };

    Lightbox.show(<TestLoadingWizard/>);
});


test("wt2. test message wizard", function() {
    return;

    class TestLoadingWizard extends Wizard {
        start() {
            this.show(<Loading>Hi, Peter!</Loading>);
            setTimeout(()=>this.loaded({name: "Peter"}), 3000);
        }

        loaded(json) {
            this.show(
                <Lightbox width='400px' closable='true'>
                    <Lightbox.Header>Header for {json.name} </Lightbox.Header>
                    <Lightbox.Body > some text some text some text some text </Lightbox.Body>
                    <Lightbox.Footer>
                        <Button>Show nested</Button>
                        <Button onClick={()=>this.go()}>Show nested2 </Button>
                    </Lightbox.Footer>
                </Lightbox>
            );

        }

        go() {
            this.show(
                <Lightbox width='400px' closable='true'>
                    <Lightbox.Header>Ste p2 </Lightbox.Header>
                    <Lightbox.Body > some text some text some text some text </Lightbox.Body>
                    <Lightbox.Footer>
                        <Button>Show nested</Button>
                        <Button onClick={()=>this.go()}>Show nested2 </Button>
                    </Lightbox.Footer>
                </Lightbox>
            );
        }
    }
    ;
    ReactDOM.render(<TestLoadingWizard/>, testEl());
});





