var $ = require("jquery");
var assert = require("assert");
var React = require("react");
var ReactDOM = require("react-dom");
var TextEditor = require("../src/textEditor");
var CheckboxEditor = require("../src/checkboxEditor");
var DateEditor = require("../src/dateEditor");
var NumEditor = require("../src/numEditor");
var Mixin = require("../src/mixin");
var m = require("coreui-models");
var Icon = require("retail-ui/components/Icon");
var Tooltip = require("coreui-forms/src/Tooltip");
var FlowManager = require("coreui-forms/src/floatPanelManager");
var TestHelper = require("./testHelper");

var _ = require("underscore");
var TestUtils = require("react-addons-test-utils");
var getTestDomElement = require("./getTestDomElement");

function setupAutoProperty() {
    var SampleModel = m.define({
        weight : m.field(),
        age : m.field().required(),
        name : m.field().validate(function(v){
            if (v=="alex")
                return v + " is invalid";
            return null;
        }),
        lastName : m.field().getter(function(){
            return this.name()+"_last";
        }).validate(function(v){
            if (v=="alex_last")
                return v + " is invalid";
            return null;
        })
    });

    var SampleForm = React.createClass({
        mixins: [Mixin],
        render: function () { return (<div>Value :
            <NumEditor form={this.forms().child('age')} className='age'/> <br/>
            <TextEditor form={this.forms().child('lastName')} className='lastName'/> <br/>
            <TextEditor form={this.forms().child('name')} className='name' />
        </div>);}
    });

    var c = getTestDomElement();
    var model = new SampleModel({name:'234'});
    ReactDOM.render((<SampleForm model={model}/>),c);
    return {
        c : c,
        model : model
    };
};

function ensureTooltip(text)
{
    assert.equal($(".ui-TooltipContainer:contains("+ text + "):visible").length, 1);
}

function ensureTooltipMissing(text)
{
    assert.equal($(".ui-TooltipContainer:contains("+ text + "):visible").length, 0);
}

function setupNumProperty() {
    var SampleModel = m.define({         age : m.field().required()    });

    var SampleForm = React.createClass({
        mixins: [Mixin],
        render: function () { return (<div>Value :
            <NumEditor form={this.forms().child('age')} className='age'/> <br/>
        </div>);}
    });
    var c = getTestDomElement();
    var model = new SampleModel({});
    ReactDOM.render((<SampleForm model={model}/>),c);
    return {        c : c,        model : model    };
};


test("ed15. render DateEditor, check value", function(){
    var SampleModel = m.define({
        date : m.field().required()
    });
    var model = new SampleModel({date: new Date("03.03.2010")});

    var SampleForm = React.createClass({
        mixins: [Mixin],
        render: function () { return (<div>Value :
            <DateEditor model={model} prop='date' className='date' /><br/>
            Year : <span className='year'>{this.model().date() ? this.model().date().getUTCFullYear() +"" : ""}</span>
        </div>);}
    });

    var c = getTestDomElement();

    var rendered = ReactDOM.render((<SampleForm model={model}/>),c);
    var dateInput = $(".date input")[0];
    TestUtils.Simulate.change(dateInput, {target: {value: '03.03.2003'}});
    TestUtils.Simulate.blur(dateInput);
    assert.equal($(".year").text(), "2003");
});


test("ed14. render num editor, check invalid format shown on bad input", function(){

    var setup = setupNumProperty();
    var c = setup.c;
    var model = setup.model;
    var ageInput = $(".age input")[0];
    ensureTooltipMissing("Поле является обязательным");
    TestUtils.Simulate.focus(ageInput);
    TestUtils.Simulate.change(ageInput, {target: {value: 'A'}});
    ensureTooltip("Неверный формат");
});


test("ed13. render num editor, check error shown on blur", function(){

    var setup = setupNumProperty();
    var c = setup.c;
    var model = setup.model;
    var ageInput = $(".age input")[0];
    ensureTooltipMissing("Поле является обязательным");
    var input = $(ageInput);
    TestUtils.Simulate.focus(ageInput);
    TestUtils.Simulate.blur(ageInput);
    TestUtils.Simulate.focus(ageInput);
    ensureTooltip("Поле является обязательным");
});

test("ed1. set initial model value, render, check input value", function(){

    var SampleModel = m.define({
        name : m.field().defaultValue("Alexander")
    });

    var SampleForm = React.createClass({
        mixins: [Mixin],
        render: function () { return (<div>
            <TextEditor form={this.forms().child('name')}/>
            Edited : {this.model().name()}
        </div>);}
    });

    var c = getTestDomElement();
    ReactDOM.render((<SampleForm model={new SampleModel()}/>),c);
    assert.equal($("input").val(), "Alexander");
});

test("ed2. render, set input value, check model value updated", function(){

    var SampleModel = m.define({
        name : m.field().defaultValue("Alexander")
    });

    var SampleForm = React.createClass({
        mixins: [Mixin],
        render: function () { return (<div>Value : <TextEditor form={this.forms().child('name')}/> </div>);}
    });

    var c = getTestDomElement();
    var model = new SampleModel({name:'234'});
    ReactDOM.render((<SampleForm model={model}/>),c);
    var input = $("input")[0];
    TestUtils.Simulate.change(input, {target: {value: 'Hello, world'}});
    assert.equal(model.name(), "Hello, world");
});

test("ed3. render two inputs, set input value, check model value updated", function(){
    var setup = setupAutoProperty();
    var c = setup.c;
    var model = setup.model;
    var nameInput = $(".name input")[0];
    var lastNameInput = $(".lastName input")[0];
    TestUtils.Simulate.change(nameInput, {target: {value: 'NewName'}});
    assert.equal(lastNameInput.value, "NewName_last");
});


test("ed4. render input for autoValue, check fx icon visible", function(cb) {
    var setup = setupAutoProperty();
    var c = setup.c;
    var iconStyle = require('retail-ui/components/Icon/Icon.less');
    new TestHelper(c).
        check("." + iconStyle.root, $icon=> {
            assert.equal($icon.text().charAt(0), '\ue036')
        }).go(cb);
});

test("ed5. edit auto value manually, check undo icon visible", function(){
    var setup = setupAutoProperty();
    var c = setup.c;
    var lastNameInput = $(".lastName input")[0];
    TestUtils.Simulate.change(lastNameInput, {target: {value: 'NewLastName'}});
    var iconStyle = require('retail-ui/components/Icon/Icon.less');
    var $icon = $(c).find("." + iconStyle.root);
    assert.equal($icon.text().charAt(0), '\ue012');

});

test("ed6. edit auto value manually, click undo icon, check value restored", function(){
    var setup = setupAutoProperty();
    var c = setup.c;
    var lastNameInput = $(".lastName input")[0];
    TestUtils.Simulate.change(lastNameInput, {target: {value: 'NewLastName'}});
    var iconStyle = require('retail-ui/Components/Icon/Icon.less');
    var $icon = $(c).find("." + iconStyle.root);
    TestUtils.Simulate.click($icon[0]);
    assert.equal($(lastNameInput).val(), "234_last");
});

test("ed7. make invalid value, check error tooltip is shown", function(){
    var setup = setupAutoProperty();
    var c = setup.c;
    var nameInput = $(".name input")[0];
    TestUtils.Simulate.change(nameInput, {target: {value: ''}});
    $(nameInput).focus();
    var iconStyle = require('retail-ui/Components/Icon/Icon.less');
    var $icon = $(c).find("." + iconStyle.root);
    TestUtils.Simulate.click($icon[0]);
    assert.equal($(nameInput).val(), "");
});

test("ed8. render form, drop form check no exceptions", function(){
        var SampleModel = m.define({
            name : m.field().required()
        });

        var SampleForm = React.createClass({
            mixins: [Mixin],
            render: function () { return (<div>Value :
                <TextEditor form={this.forms().child('name')} className='name' />
            </div>);}
        });

    var c = getTestDomElement();
    var model = new SampleModel({name:''});

    var rendered = ReactDOM.render((<SampleForm model={model}/>),c);
    var nameInput = $(".name input")[0];
    TestUtils.Simulate.change(nameInput, {target: {value: 'A'}});
    TestUtils.Simulate.change(nameInput, {target: {value: ''}});
    $(nameInput).focus();

    ReactDOM.unmountComponentAtNode(c);
    var rendered = ReactDOM.render((<SampleForm model={model}/>),c);
});


test("ed9. render form, drop form check no exceptions", function(){
    var SampleModel = m.define({
        name : m.field().required(),
        superName : m.field().getter(function(){
            return this.name() + "-super";
        }).validate((v)=>v && v.length>10 ? "too long" : null)
    });

    var SampleForm = React.createClass({
        mixins: [Mixin],
        render: function () { return (<div>Value :
            <TextEditor form={this.forms().child('name')} className='name' />
            <TextEditor form={this.forms().child('superName')} className='superName' />
        </div>);}
    });

    var c = getTestDomElement();
    var model = new SampleModel({name:''});

    var rendered = ReactDOM.render((<SampleForm model={model}/>),c);
    var nameInput = $(".name input")[0];
    var superNameInput = $(".superName input")[0];
    TestUtils.Simulate.change(nameInput, {target: {value: 'werwerwerwerwer'}});
    $(superNameInput).focus();

    TestUtils.Simulate.focus(superNameInput);
    assert.equal($(".ui-TooltipContainer:contains(too long):visible").length, 1);

    TestUtils.Simulate.blur(superNameInput);
    assert.equal($(".ui-TooltipContainer:contains(too long):visible").length, 0);
});

test("ed10. render checkbox, check initial value set", function(){
    var SampleModel = m.define({
        isGood : m.field().defaultValue(true)
    });
    var SampleForm = React.createClass({
        mixins: [Mixin],
        render: function () { return (<div>Value :
            <CheckboxEditor form={this.forms().child('isGood')} className='isGood' />
            <div>IsGood : {this.model().isGood() +"" }</div>
        </div>);}
    });

    var c = getTestDomElement();
    var model = new SampleModel({});

    var rendered = ReactDOM.render((<SampleForm model={model}/>),c);
    var isGoodInput = $("input.name")[0];
});

test("ed11. render checkbox, check updated value set", function(){
    var SampleModel = m.define({
        isGood : m.field().defaultValue(false)
    });
    var model = new SampleModel({});
    var SampleForm = React.createClass({
        render: function () { return (<div>Value :
            <CheckboxEditor model={model} prop='isGood' className='isGood' />
        </div>);}
    });

    var c = getTestDomElement();
    var rendered = ReactDOM.render((<SampleForm model={model}/>),c);
    model.isGood(true);
});

test("ed12. render DateEditor, check no exceptions", function(){
    var SampleModel = m.define({
        date : m.field().required()
    });
    var model = new SampleModel({date: new Date("01.01.2010")});
    var SampleForm = React.createClass({
        mixins: [Mixin],
        render: function () { return (<div>Value :
            <DateEditor model={model} prop='date' className='date' /><br/>
            Value : {this.model().date() +""}
        </div>);}
    });

    var c = getTestDomElement();
    var rendered = ReactDOM.render((<SampleForm model={model}/>),c);
    var editor = $(c).find("input");
    assert.equal(editor.val(), "01.01.2010");
});